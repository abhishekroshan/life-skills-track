# Prevention of Sexual Harassment

## What kinds of behavior cause sexual harassment?

Sexual harassment includes any unwelcome verbal, visual, or physical conduct of a sexual nature that is severe or pervasive and affects working conditions or creates a hostile work environment.

These behaviors can include but are not limited to:

* Comments about clothing.
* Comments about a person's body.
* Obscene posters.
* Drawing/Pictures.
* Sexual assaults.
* Impeding or blocking movements.
* Inappropriate Touching.

## What would you do if you faced or witnessed any incident or repeated incidents of such behavior?

* Record specific details such as date, time, location, and individuals involved.
* Include any relevant information about the nature of the harassment.
* communicate to the harasser that their behavior is unwelcome.
* Assertively ask them to stop the inappropriate conduct.
* Familiarize yourself with your organization's policies on reporting sexual harassment.
* Follow the appropriate channels to report the incident, providing documented information.
* Save any relevant messages, emails, or documents that may serve as evidence.
* Familiarize yourself with local laws and regulations regarding sexual harassment.
* Seek legal advice to understand your rights and potential courses of action.