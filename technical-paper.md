# Scaling

## Problem Statement

You joined a new project. The project is going through some performance and scaling issues. After some analysis, the team lead asks you to investigate the possibility of using load balancers and understand vertical/horizontal scaling solutions.

## Solution Approach

Whenever you see an increase in traffic in your application, the machines and servers can't handle large amounts of requests anymore and it may fail soon.
There are two types of approach to solve the problem.

1. Vertical Scaling
2. Horizontal Scaling

![Scaling-concept](/Scaling-Concept.png)

## Vertical Scaling

Upgrading the capacity of a single machine or moving to a new machine with more power is called vertical scaling.
Vertical scaling can be easily achieved by switching from small to bigger machines.

### Advantages of Vertical Scaling

1. Simple to implement

2. Easier Management

3. Data consistent

### Disadvantages of vertical scaling

1. Limited scalability

2. Risk of high downtime

3. Single point of failure

4. Harder to upgrade

## Horizontal Scaling

This type of scaling involves adding more resources or nodes to a distributed system. As the user load increases, additional servers or nodes are introduced, which can share the workload and distribute requests across the system. Horizontal scalability is commonly associated with the idea of “scaling out” and is preferred for handling massive concurrent users.

### Advantages of horizontal scaling

1. Increased capacity

2. Improved performance

3. Increased fault tolerance

4. Easier to upgrade

### Disadvantages of horizontal scaling:

1. Increased Complexity

2. Data Inconsistency

## Differences between Vertical and Horizontal Scaling

1. Load Balancing: Horizontal scaling requires load balancing to distribute or spread the traffic among several machines. In the vertical machine, there is just one machine to handle the load so it doesn’t require a load balancer.

2. Failure Resilience: Horizontal scaling is more resistant to system failure. If one of the machines fails you can redirect the request to another machine and the application won’t face downtime. This is not in the case of vertical scaling, it has a single machine so it will have a single point of failure. This simply means in horizontal scaling you can achieve availability more easily than in vertical scaling.

3. Data Consistency: Data is inconsistent in horizontal scaling because different machines handle different requests which may lead to their data becoming out of sync which must be addressed. On the other side, vertical machines have just one single machine where all the requests will be redirected, so there is no issue of inconsistency of data in vertical scaling.

4. Limitations: Depends on budget, space, or requirement you can add as many servers as you want in horizontal scaling and scales your application as much as you want. This is not in the case of vertical scaling. There is a finite limit to the capacity achievable with vertical scaling.

## Load Balancers

If you develop a popular web solution, you will face huge traffic with a huge amount of requests from your users. You will scale your solution by adding more and more servers for processing those requests. To be able to distribute all of them you will need a load balancer.

The job of a load balancer is to divide clients’ requests among the pool of available servers. And do it the way to avoid crashing or overloading the last ones.

![Load-Balancer](/1%20W_bepkloBvEeuIs67N2pSQ.png)

## Conclusion

To tackle the scaling issues load balancers can be a effective way to solution. We can distribute the client requests to multiple number of servers so that all the servers are equally used without worrying about the failure, performance and scaling problem. And if we need more scaling power we can just add more servers and the load balancers will tackle the issue.

## Reference

- [Horizontal and Vertical Scaling | System Design](https://www.geeksforgeeks.org/system-design-horizontal-and-vertical-scaling/)

- [Scaling Up: The Art of System Design Scalability](https://levelup.gitconnected.com/scaling-up-the-art-of-system-design-scalability-93809d4f1b68)

- [System Design BASICS: Horizontal vs. Vertical Scaling](https://www.youtube.com/watch?v=xpDnVSmNFX0)

- [Horizontal vs. Vertical Scaling](https://www.youtube.com/watch?v=3GGPsX-s5-M)
